

document.getElementById('getBooks').onclick = function() {
	var newList = document.createElement('ul');
	var xhttp1 = new XMLHttpRequest();
	xhttp1.open("GET", "http://localhost:64591/Project1/webresources/Book", true);
	//xhttp1.setRequestHeader( 'Access-Control-Allow-Origin', '*');
	xhttp1.onreadystatechange = function(){
		if(xhttp1.readystatechange === 4 && xhttp1.status === 200){
			var res = JSON.parse(this.responseText);
			document.innerHTML = res;
		}
	};
	xhttp1.send();

	//for(i=0; i < 10; i++){
	//	var newItem = document.createElement('li');
	//	var text = document.createTextNode('List content');
	//	newItem.appendChild(text);
	//	newList.appendChild(newItem);
	//}
	//document.getElementById('bookmain').appendChild(newList);
	//document.body.appendChild(newList);
};

document.getElementById('addBook').onclick = function() {
	var ntitle = document.getElementById("title").value;
	var npages = document.getElementById("pages").value;
	var nauthor = document.getElementById("authorb").value;
	var xhttp2 = new XMLHttpRequest();
	xhttp2.open("POST", "http://localhost:64591/Project1/webresources/Book/insert/"+ ntitle+"/"+npages+"/"+nauthor, true);
	xhttp2.send();
};


document.getElementById('updateBook').onclick = function() {
	var updateid = document.getElementById("updateid").value;
	var updtitle = document.getElementById("updtitle").value;
	//alert(updateid);
	//alert(updtitle);
	var xhttp3 = new XMLHttpRequest();
	xhttp3.open("PUT","http://localhost:64591/Project1/webresources/Book/update/"+updateid+"/"+updtitle, true);
	xhttp3.onreadystatechange = function(){
		if(xhttp3.readyState === 4 && xhttp3.status === 200){
			alert("Success");
		}
	};
	xhttp3.send();
};

document.getElementById('deleteBook').onclick = function() {
	var deleteid = document.getElementById("deleteid").value;
	var xhttp4 = new XMLHttpRequest();
	xhttp4.open("DELETE", "http://localhost:64591/Project1/webresources/Book/delete/"+deleteid, true);
	xhttp4.send();
};


document.getElementById('getAuthors').onclick = function() {
	var anewList = document.createElement('ul');
	for(i=0; i < 10; i++){
		var anewItem = document.createElement('li');
		var atext = document.createTextNode('List content');
		anewItem.appendChild(atext);
		anewList.appendChild(anewItem);
	}
	document.getElementById('authormain').appendChild(anewList);
	//document.body.appendChild(anewList);
};

document.getElementById('addAuthor').onclick = function() {
	var fname = document.getElementById("fname").value;
	var lname = document.getElementById("lname").value;
	var age = document.getElementById("age").value;

	var xhttp5 = new XMLHttpRequest();
	xhttp5.open("POST", "http://localhost:64591/Project1/webresources/Author/insert/"+ fname+"/"+lname+"/"+age, true);
	xhttp5.send();
};

document.getElementById('updateAuthor').onclick = function() {
	var updateaid = document.getElementById("updateaid").value;
	var updateaage = document.getElementById("updateaage").value;

	var xhttp6 = new XMLHttpRequest();
	xhttp6.open("PUT","http://localhost:64591/Project1/webresources/Author/update/"+updateaid+"/"+updateaage, true);
	xhttp6.send();
};

document.getElementById('deleteAuthor').onclick = function() {
	var deleteaid = document.getElementById("deleteaid").value;
	var xhttp7 = new XMLHttpRequest();
	xhttp7.open("DELETE", "http://localhost:64591/Project1/webresources/Author/delete/"+deleteaid, true);
	xhttp7.send();
};
//-------------------------------------------------------------------------
document.getElementById('lotr').onclick = function() {
	var xhttp = new XMLHttpRequest();

	xhttp.open("GET", "https://www.googleapis.com/books/v1/volumes?q=lordoftherings&maxResults=10",true );
	xhttp.onreadystatechange = function(){
		if(xhttp.readyState === 4 && xhttp.status === 200){
			var arr = JSON.parse(this.responseText);
			document.getElementById("lotr").innerHTML = arr;
			console.log(arr.items);
			for(i = 0; i < arr.items.length; i++){
				var tempB = new Book('tempB');
				var tempA = new Author('tempA');
				tempB.title = arr.items[i].volumeInfo.title;
				tempB.pages = arr.items[i].volumeInfo.pageCount;
				tempB.author  = arr.items[i].volumeInfo.authors;

				var temp1 = arr.items[i].volumeInfo.authors.toString();
				var temp2 = temp1.split(" ");
				tempA.firstname = temp2[0];
				tempA.lastname = temp2[1];

				var xhttpb = new XMLHttpRequest();
				xhttpb.open("POST", "http://localhost:64591/Project1/webresources/Book/insert/"+tempB.title+"/"+tempB.pages+"/"+tempB.author, true);
				alert("http://localhost:64591/Project1/webresources/Book/insert/"+tempB.title+"/"+tempB.pages+"/"+tempB.author);
				xhttpb.onreadystatechange = function(){
					//alert("hello");
				}
				xhttpb.send();

				var xhttpa = new XMLHttpRequest();
				xhttpa.open("POST", "http://localhost:64591/Project1/webresources/Author/insert/"+tempA.firstname+"/"+tempA.lastname+"/"+tempA.age, true);
				alert("http://localhost:64591/Project1/webresources/Author/insert/"+tempA.firstname+"/"+tempA.lastname+"/"+tempA.age);
				xhttpa.onreadystatechange = function(){
					//alert("hello");
				}
				xhttpa.send();
				//alert("hello");
			}

		}
	};
		xhttp.send();
	
};

// objects
function Book(ntitle, npages, nauthor) {
	this.title = ntitle;
	this.pages = npages;
	this.author = nauthor;
};

//Book.prototype.title = "default title";
//Book.prototype.pages = 0;
//Book.prototype.author = "default author";

Book.prototype.getTitle = function() {};
Book.prototype.getPages = function() {};
Book.prototype.getAuthor = function() {};

Book.prototype.setTitle = function(newTitle) {};
Book.prototype.setPages = function(newPages) {};
Book.prototype.setAuthor = function(newAuthor) {};


function Author(nfname, nlname) {
	this.firstname = nfname;
	this.lastname = nlname;
	this.age = 0;
};



//Author.prototype.firstname = "default first";
//Author.prototype.lastname = "default last";
//Author.prototype.age = 0;

Author.prototype.getFirstname = function(){};
Author.prototype.getLastname = function(){};
Author.prototype.getAge = function(){};

Author.prototype.setFirstname = function(newFname){};
Author.prototype.setLname = function(newLname){};
Author.prototype.setAge = function(newAge){};


